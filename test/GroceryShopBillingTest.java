import org.junit.Test;

import java.util.*;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class GroceryShopBillingTest {
    List<Item> cart = new ArrayList<Item>();
    @Test
    public void billWithTotalBeZero() {
       Item item=new Item(00001,0);
       assertEquals (0, item.totalPrice(),0.01);
    }
    @Test
    public void billWithTotalBe200ForOneCoconutOil() {
        Item item=new Item(00001,1);
        float ActualAmount=item.totalPrice();
        assertEquals (200,ActualAmount,0.01);
    }
    @Test
    public void billWithTaxForEatable() {
        Item coconutOil=new Item(00001,1);
        Item rice=new Item(00002,1);
        cart.add(coconutOil);
        cart.add(rice);
        Bill bill=new Bill(cart);

        float ActualAmount=bill.totalBill();

        assertEquals (273,ActualAmount,0.01);
    }
    @Test
    public void billWithTaxForDailyUsage() {
        Item toothPaste=new Item(00005,2);
        cart.add(toothPaste);
        Bill bill=new Bill(cart);

        float ActualAmount=bill.totalBill();

        assertEquals (97.74,ActualAmount,0.5);
    }
    @Test
    public void billWithTaxForCosmetics() {

        Item makeupKit=new Item(00006,2);
        cart.add(makeupKit);
        Bill bill=new Bill(cart);

        float ActualAmount=bill.totalBill();

        assertEquals (679.65,ActualAmount,0.50);
      }
      @Test
      public void billWithTaxForDifferentItems() {
        Item coconutOil=new Item(00001,1f);
        Item dhal=new Item(00004,1.5f);
        Item makeupKit=new Item(00006,2f);
        cart.add(coconutOil);
        cart.add(dhal);
        cart.add(makeupKit);
        Bill bill=new Bill(cart);

        float ActualAmount=bill.totalBill();

        assertEquals (1079.430,ActualAmount,0.5);
      }
    @Test
    public void billWithDiscountForBillAmountMoreThan1000AndLessThan2000() {
        Item coconutOil=new Item(00001,1f);
        Item dhal=new Item(00004,1.5f);
        Item makeupKit=new Item(00006,2f);
        cart.add(coconutOil);
        cart.add(dhal);
        cart.add(makeupKit);
        Bill bill=new Bill(cart);

        float ActualAmount=bill.calculateDiscount();

        assertEquals (1057.80,ActualAmount,1.0);
    }
    @Test
    public void billWithDiscountForBillAmountMoreThan2000() {
        Item coconutOil=new Item(00001,4f);
        Item rice=new Item(00002,4);
        Item dhal=new Item(00004,1.5f);
        Item toothPaste=new Item(00005,2);
        Item makeupKit=new Item(00006,2f);
        cart.add(coconutOil);
        cart.add(rice);
        cart.add(dhal);
        cart.add(toothPaste);
        cart.add(makeupKit);

        Bill bill=new Bill(cart);

        float ActualAmount=bill.calculateDiscount();

        assertEquals (1956.20,ActualAmount,1.00);
    }
    @Test
    public void billWithTaxForCosmeticsWithSpecialDiscount() {

        Item makeupKit=new Item(00006,2);
        cart.add(makeupKit);
        Bill bill=new Bill(cart);

        float ActualAmount=bill.totalBill();

        assertEquals (679.65,ActualAmount,0.50);
    }
    @Test
    public void billForTotalSales() {
        Bill bill=new Bill(cart);
        float ActualAmount=bill.prevBillAmount;
        assertEquals (0,ActualAmount,0.50);
    }

}
