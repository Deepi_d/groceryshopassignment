import java.util.*;
public class Bill {
    float billAmount;
    static float prevBillAmount=0;
    List<Item> list = new ArrayList<Item>();

    public Bill(List<Item> list) {
        this.list = list;
    }

    public  float totalBill() {
        billAmount=0;
        for (Item item:list){
            billAmount+=item.priceWithTax(item.code);
        }
        prevBillAmount+=billAmount;
        System.out.println(prevBillAmount);
        return billAmount;

    }
    public float calculateDiscount()
    {
        billAmount=totalBill();
        float sum=0;
            if ((billAmount > 1000) && (billAmount < 2000)) {
                sum = billAmount - (0.02f * billAmount);
            } else if (billAmount > 2000) {
                sum = billAmount - (0.05f * billAmount);
            }
      return sum;
    }

}
