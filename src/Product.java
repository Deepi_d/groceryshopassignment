public class Product {
    long itemCode;
    String itemName;
    String type;
    Float price;

    public Product(long itemCode, String itemName, String type, Float price) {
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.type = type;
        this.price = price;
    }
}
