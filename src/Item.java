import java.util.*;

public class Item {

    float quantity;
    long code;
    List <Product>  productList = new ArrayList<Product>();
    float totalPrice;
    float priceWithTax;

    public Item(long code, float quantity) {
        this.quantity = quantity;
        this.code=code;
    }
    public void createItems (){

        Product p1=new Product(00001,"COCONUT OIL", "Eatable" ,200.00f);
        Product p2=new Product(00002,"RICE", "Eatable" ,60.00f);
        Product p3=new Product(00004,"DHAL", "Eatable" ,120.50f);
        Product p4=new Product(00005,"Tooth paste", "Daily Usable" ,45.25f);
        Product p5=new Product(00006,"Makeup kit", "Cosmetic" ,  300.00f);
        productList.add(p1);
        productList.add(p2);
        productList.add(p3);
        productList.add(p4);
        productList.add(p5);
    }
    public float totalPrice() {
        this.createItems();
        for (Product product:productList){
            if(product.itemCode==this.code)
            {
                totalPrice =product.price*this.quantity;
            }
        }
        return totalPrice;
    }
    public float priceWithTax(long itemCode) {
        totalPrice=totalPrice();
         priceWithTax = 0;
         String type = null;
                if((itemCode==00001)||(itemCode==00002)||(itemCode==00004) ){
                    priceWithTax = totalPrice + 0.05f * totalPrice;
                }
                else if(itemCode==00005) {
                    priceWithTax = totalPrice + 0.08f * totalPrice;
                }
                else if(itemCode==00006) {
                    priceWithTax = totalPrice + (0.15f * totalPrice);
                    priceWithTax= priceWithTax - (0.015f*priceWithTax)   ;
                }
        return priceWithTax;
    }

}
